/* eslint-disable import/prefer-default-export */
export {default as FarmersBaseForm } from './FarmersBaseForm.vue';
export {default as BuyerBaseForm } from './BuyerBaseForm.vue';
export {default as ServiceProviderBaseForm } from './ServiceProviderBaseForm.vue';