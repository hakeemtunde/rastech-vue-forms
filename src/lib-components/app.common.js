import axios from 'axios'

const appCommonService = {
    postData(url, data) {
        return new Promise((resolve, reject) => {
            axios
                .post(url, data)
                .then(response => {
                    resolve(response.data)
                })
                .catch(error => {
                    reject(error)
                });
        })
    }
}

export default appCommonService
