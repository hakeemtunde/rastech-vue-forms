import Vue from 'vue';
import Dev from './serve.vue';

Vue.config.productionTip = false;

const VueFormulate = require('@braid/vue-formulate')
Vue.use(VueFormulate.default)

new Vue({
  render: (h) => h(Dev),
}).$mount('#app');
