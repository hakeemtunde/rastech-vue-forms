# Rastechng client form components!
Decouple clients forms

## Integration into Laravel

### NPM dependencies (packages.json)
```
npm install vue vue-template-compiler @braid/vue-formulate axios rastech-forms
```

### bootstrap.js

> window.Vue = require('vue')
> window.axios = require('axios')

### Use vue-formulate form plugin

> const  VueFormulate = require('@braid/vue-formulate');
> Vue.use(VueFormulate.default);

### app.js

```
import {BuyerBaseForm} from  'rastech-forms'
import {FarmersBaseForm} from  'rastech-forms'
Vue.component('buyer-base-form', BuyerBaseForm);
Vue.component('farmers-base-form', FarmersBaseForm)

const  app = new  Vue({
    el:  '#rastech-form'
});
```

### Build
``` 
npm run dev
```

###  css(Offline) - copy/paste into public
```https://github.com/wearebraid/vue-formulate/blob/master/dist/snow.min.css```

### Using form component in view

```
<div class="card-body" id="rastech-form">
     <farmers-base-form 
           server-url="{{ env('RASTECH_URL') }}" 
           client-key="{{ env('RASTECH_CLIENT_API_KEY') }}" >
     </farmers-base-form>
</div>
```
### include app.js and .css in view

```
<link href="https://github.com/wearebraid/vue-formulate/blob/master/dist/snow.min.css" rel="stylesheet"> 
```
``` 
<script src="{{asset('js/app.js') }}" type="text/javascript"></script>
```