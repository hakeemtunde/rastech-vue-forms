'use strict';Object.defineProperty(exports,'__esModule',{value:true});var axios=require('axios');function _interopDefaultLegacy(e){return e&&typeof e==='object'&&'default'in e?e:{'default':e}}var axios__default=/*#__PURE__*/_interopDefaultLegacy(axios);function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it;

  if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function () {};

      return {
        s: F,
        n: function () {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function (e) {
          throw e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function () {
      it = o[Symbol.iterator]();
    },
    n: function () {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function (e) {
      didErr = true;
      err = e;
    },
    f: function () {
      try {
        if (!normalCompletion && it.return != null) it.return();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}var appCommonService = {
  postData: function postData(url, data) {
    return new Promise(function (resolve, reject) {
      axios__default['default'].post(url, data).then(function (response) {
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
};var script = {
  name: 'FarmersBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function data() {
    return {
      gender: '',
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      responseFarmerId: '',
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit: function handleSubmit(data) {
      var _this = this;

      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      var farmerData = {
        sureName: data['surname'],
        firstName: data['First Name'],
        middleName: data['Middle name'],
        gender: data['Gender'],
        homeAddress: data['Address'],
        phoneNumber: data['phone no'],
        email: data['email'],
        password: data['password'],
        password_confirmation: data['password_confirm'],
        title: data['title'],
        client_key: this.clientKey
      };
      appCommonService.postData(this.serverUrl + '/api/clientfarmers', farmerData).then(function (response) {
        console.log(response);
        _this.responseFarmerId = response.farmer_id;
        _this.showForm = false;
        _this.showResponse = true;
        _this.disabledBtn = false;
        _this.submitText = 'Submit';
      }).catch(function (error) {
        console.log(error.response.data.errors);
        _this.disabledBtn = false;
        _this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              _this.mapErrors(error.response.data.errors);

          }
        }
      });
    },
    mapErrors: function mapErrors(errors) {
      this.formErrors = [];

      for (var _i = 0, _Object$entries = Object.entries(errors); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            val = _Object$entries$_i[1];

        this.formErrors.push(val[0]);
      }
    }
  }
};function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}function createInjectorSSR(context) {
    if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
    }
    if (!context)
        return () => { };
    if (!('styles' in context)) {
        context._styles = context._styles || {};
        Object.defineProperty(context, 'styles', {
            enumerable: true,
            get: () => context._renderStyles(context._styles)
        });
        context._renderStyles = context._renderStyles || renderStyles;
    }
    return (id, style) => addStyle(id, style, context);
}
function addStyle(id, css, context) {
    const group =  css.media || 'default' ;
    const style = context._styles[group] || (context._styles[group] = { ids: [], css: '' });
    if (!style.ids.includes(id)) {
        style.media = css.media;
        style.ids.push(id);
        let code = css.source;
        style.css += code + '\n';
    }
}
function renderStyles(styles) {
    let css = '';
    for (const key in styles) {
        const style = styles[key];
        css +=
            '<style data-vue-ssr-id="' +
                Array.from(style.ids).join(' ') +
                '"' +
                (style.media ? ' media="' + style.media + '"' : '') +
                '>' +
                style.css +
                '</style>';
    }
    return css;
}/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_vm._ssrNode("<div class=\"form-wrapper\"" + _vm._ssrStyle(null, null, {
    display: _vm.showResponse ? '' : 'none'
  }) + "><p>Profile was created, your farmer ID: </p> <h2>" + _vm._ssrEscape(_vm._s(_vm.responseFarmerId)) + "</h2></div> "), _c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    },
    model: {
      value: _vm.formData,
      callback: function callback($$v) {
        _vm.formData = $$v;
      },
      expression: "formData"
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Farmer's enrolment form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "label": "Title",
      "name": "title",
      "placeholder": "Title",
      "type": "select",
      "options": [{
        value: 'Alh',
        label: 'Alhaji'
      }, {
        value: 'Mr',
        label: 'Mr.'
      }, {
        value: 'Mrs',
        label: 'Mrs.'
      }, {
        value: 'Miss',
        label: 'Miss'
      }, {
        value: 'Mal',
        label: 'Mallam'
      }],
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "First Name",
      "type": "text",
      "label": "First name",
      "placeholder": "First name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Middle name",
      "type": "text",
      "label": "Middle name",
      "placeholder": "Middle name"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "surname",
      "type": "text",
      "label": "Last name",
      "placeholder": "Last name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Gender",
      "options": {
        male: 'Male',
        female: 'Female'
      },
      "type": "radio",
      "label": "Gender"
    },
    model: {
      value: _vm.gender,
      callback: function callback($$v) {
        _vm.gender = $$v;
      },
      expression: "gender"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "text",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "double-wide"
  }, [_c('FormulateInput', {
    attrs: {
      "name": "password",
      "type": "password",
      "label": "Password",
      "placeholder": "Your password",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "password_confirm",
      "type": "password",
      "label": "Confirm your password",
      "placeholder": "Confirm password",
      "validation": "required|confirm",
      "validation-name": "Confirmation"
    }
  })], 1), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 2);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-71cbb2b8_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = undefined;
/* module identifier */

var __vue_module_identifier__ = "data-v-71cbb2b8";
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, createInjectorSSR, undefined);var script$1 = {
  name: 'BuyerBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function data() {
    return {
      gender: '',
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit: function handleSubmit(data) {
      var _this = this;

      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      var farmerData = {
        username: data['username'],
        sureName: data['surname'],
        firstName: data['First Name'],
        middleName: data['Middle name'],
        gender: data['Gender'],
        address: data['Address'],
        phoneNumber: data['phone no'],
        email: data['email'],
        password: data['password'],
        password_confirmation: data['password_confirm'],
        title: data['title'],
        client_key: this.clientKey
      };
      appCommonService.postData(this.serverUrl + '/api/clientbuyers', farmerData).then(function (response) {
        console.log(response);
        _this.responseFarmerId = response.farmer_id;
        _this.showForm = false;
        _this.showResponse = true;
        _this.disabledBtn = false;
        _this.submitText = 'Submit';
      }).catch(function (error) {
        console.log(error.response.data.errors);
        _this.disabledBtn = false;
        _this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              _this.mapErrors(error.response.data.errors);

          }
        }
      });
    },
    mapErrors: function mapErrors(errors) {
      this.formErrors = [];

      for (var _i = 0, _Object$entries = Object.entries(errors); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            val = _Object$entries$_i[1];

        this.formErrors.push(val[0]);
      }
    }
  }
};/* script */
var __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_vm._ssrNode("<div class=\"form-wrapper\"" + _vm._ssrStyle(null, null, {
    display: _vm.showResponse ? '' : 'none'
  }) + "><h3>Profile was created </h3></div> "), _c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    },
    model: {
      value: _vm.formData,
      callback: function callback($$v) {
        _vm.formData = $$v;
      },
      expression: "formData"
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Buyer sign up form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "label": "Title",
      "name": "title",
      "placeholder": "Title",
      "type": "select",
      "options": [{
        value: 'Alh',
        label: 'Alhaji'
      }, {
        value: 'Mr',
        label: 'Mr.'
      }, {
        value: 'Mrs',
        label: 'Mrs.'
      }, {
        value: 'Miss',
        label: 'Miss'
      }, {
        value: 'Mal',
        label: 'Mallam'
      }],
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "First Name",
      "type": "text",
      "label": "First name",
      "placeholder": "First name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Middle name",
      "type": "text",
      "label": "Middle name",
      "placeholder": "Middle name"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "surname",
      "type": "text",
      "label": "Last name",
      "placeholder": "Last name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Gender",
      "options": {
        male: 'Male',
        female: 'Female'
      },
      "type": "radio",
      "label": "Gender"
    },
    model: {
      value: _vm.gender,
      callback: function callback($$v) {
        _vm.gender = $$v;
      },
      expression: "gender"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "text",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "username",
      "type": "text",
      "label": "Username",
      "placeholder": "Username",
      "validation": "required"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "double-wide"
  }, [_c('FormulateInput', {
    attrs: {
      "name": "password",
      "type": "password",
      "label": "Password",
      "placeholder": "Your password",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "password_confirm",
      "type": "password",
      "label": "Confirm your password",
      "placeholder": "Confirm password",
      "validation": "required|confirm",
      "validation-name": "Confirmation"
    }
  })], 1), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 2);
};

var __vue_staticRenderFns__$1 = [];
/* style */

var __vue_inject_styles__$1 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-25a704ed_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$1 = undefined;
/* module identifier */

var __vue_module_identifier__$1 = "data-v-25a704ed";
/* functional template */

var __vue_is_functional_template__$1 = false;
/* style inject shadow dom */

var __vue_component__$1 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, undefined, createInjectorSSR, undefined);var INPUT_PROVIDER = 'INPUT_PROVIDER',
    OFFTAKER = 'OFF_TAKER',
    MECHANIZATION = 'MECHANIZATION_PROVIDER';
var script$2 = {
  name: 'ServiceProviderBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function data() {
    return {
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      operations: [],
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit: function handleSubmit(data) {
      var _this = this;

      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      var farmerData = {
        name: data['company name'],
        address: data['Address'],
        phone: data['phone no'],
        email: data['email'],
        client_key: this.clientKey
      };

      var _iterator = _createForOfIteratorHelper(this.operations),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var operation = _step.value;

          if (operation == INPUT_PROVIDER) {
            farmerData.input_provider = INPUT_PROVIDER;
          }

          if (operation == OFFTAKER) {
            farmerData.off_taker = OFFTAKER;
          }

          if (operation == MECHANIZATION) {
            farmerData.mechanization = MECHANIZATION;
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      appCommonService.postData(this.serverUrl + '/api/clientserviceproviders', farmerData).then(function (response) {
        console.log(response);
        _this.responseFarmerId = response.farmer_id;
        _this.showForm = false;
        _this.showResponse = true;
        _this.disabledBtn = false;
        _this.submitText = 'Submit';
      }).catch(function (error) {
        console.log(error.response.data.errors);
        _this.disabledBtn = false;
        _this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              _this.mapErrors(error.response.data.errors);

          }
        }
      });
    },
    mapErrors: function mapErrors(errors) {
      this.formErrors = [];

      for (var _i = 0, _Object$entries = Object.entries(errors); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            val = _Object$entries$_i[1];

        this.formErrors.push(val[0]);
      }
    }
  }
};/* script */
var __vue_script__$2 = script$2;
/* template */

var __vue_render__$2 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_vm._ssrNode("<div class=\"form-wrapper\"" + _vm._ssrStyle(null, null, {
    display: _vm.showResponse ? '' : 'none'
  }) + "><h3>Company profile was created </h3></div> "), _vm._ssrNode("<div>", "</div>", [_c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Service provider sign up form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "company name",
      "type": "text",
      "label": "Company Name",
      "placeholder": "Company name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "textarea",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "options": {
        INPUT_PROVIDER: 'INPUT PROVIDERS',
        'OFF_TAKER': 'OFFTAKER',
        'MECHANIZATION_PROVIDER': 'MECHANIZATION PROVIDER'
      },
      "type": "checkbox",
      "label": "Company operation(s)"
    },
    model: {
      value: _vm.operations,
      callback: function callback($$v) {
        _vm.operations = $$v;
      },
      expression: "operations"
    }
  }), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 1)], 2);
};

var __vue_staticRenderFns__$2 = [];
/* style */

var __vue_inject_styles__$2 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-b89d7a94_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$2 = undefined;
/* module identifier */

var __vue_module_identifier__$2 = "data-v-b89d7a94";
/* functional template */

var __vue_is_functional_template__$2 = false;
/* style inject shadow dom */

var __vue_component__$2 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$2,
  staticRenderFns: __vue_staticRenderFns__$2
}, __vue_inject_styles__$2, __vue_script__$2, __vue_scope_id__$2, __vue_is_functional_template__$2, __vue_module_identifier__$2, false, undefined, createInjectorSSR, undefined);/* eslint-disable import/prefer-default-export */var components=/*#__PURE__*/Object.freeze({__proto__:null,FarmersBaseForm: __vue_component__,BuyerBaseForm: __vue_component__$1,ServiceProviderBaseForm: __vue_component__$2});var install = function installRastechForms(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        componentName = _ref2[0],
        component = _ref2[1];

    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


var plugin = {
  install: install
}; // To auto-install on non-es builds, when vue is found
// eslint-disable-next-line no-redeclare

/* global window, global */

{
  var GlobalVue = null;

  if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
  } else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
  }

  if (GlobalVue) {
    GlobalVue.use(plugin);
  }
} // Default export is library as a whole, registered via Vue.use()
exports.BuyerBaseForm=__vue_component__$1;exports.FarmersBaseForm=__vue_component__;exports.ServiceProviderBaseForm=__vue_component__$2;exports.default=plugin;