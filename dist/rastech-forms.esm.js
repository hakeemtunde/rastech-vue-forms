import axios from 'axios';

const appCommonService = {
  postData(url, data) {
    return new Promise((resolve, reject) => {
      axios.post(url, data).then(response => {
        resolve(response.data);
      }).catch(error => {
        reject(error);
      });
    });
  }

};

//
var script = {
  name: 'FarmersBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function () {
    return {
      gender: '',
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      responseFarmerId: '',
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit(data) {
      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      const farmerData = {
        sureName: data['surname'],
        firstName: data['First Name'],
        middleName: data['Middle name'],
        gender: data['Gender'],
        homeAddress: data['Address'],
        phoneNumber: data['phone no'],
        email: data['email'],
        password: data['password'],
        password_confirmation: data['password_confirm'],
        title: data['title'],
        client_key: this.clientKey
      };
      appCommonService.postData(this.serverUrl + '/api/clientfarmers', farmerData).then(response => {
        console.log(response);
        this.responseFarmerId = response.farmer_id;
        this.showForm = false;
        this.showResponse = true;
        this.disabledBtn = false;
        this.submitText = 'Submit';
      }).catch(error => {
        console.log(error.response.data.errors);
        this.disabledBtn = false;
        this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              this.mapErrors(error.response.data.errors);
          }
        }
      });
    },

    mapErrors(errors) {
      this.formErrors = [];

      for (const [key, val] of Object.entries(errors)) {
        this.formErrors.push(val[0]);
      }
    }

  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
const __vue_script__ = script;
/* template */

var __vue_render__ = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showResponse,
      expression: "showResponse"
    }],
    staticClass: "form-wrapper"
  }, [_c('p', [_vm._v("Profile was created, your farmer ID: ")]), _vm._v(" "), _c('h2', [_vm._v(_vm._s(_vm.responseFarmerId))])]), _vm._v(" "), _c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    },
    model: {
      value: _vm.formData,
      callback: function ($$v) {
        _vm.formData = $$v;
      },
      expression: "formData"
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Farmer's enrolment form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "label": "Title",
      "name": "title",
      "placeholder": "Title",
      "type": "select",
      "options": [{
        value: 'Alh',
        label: 'Alhaji'
      }, {
        value: 'Mr',
        label: 'Mr.'
      }, {
        value: 'Mrs',
        label: 'Mrs.'
      }, {
        value: 'Miss',
        label: 'Miss'
      }, {
        value: 'Mal',
        label: 'Mallam'
      }],
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "First Name",
      "type": "text",
      "label": "First name",
      "placeholder": "First name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Middle name",
      "type": "text",
      "label": "Middle name",
      "placeholder": "Middle name"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "surname",
      "type": "text",
      "label": "Last name",
      "placeholder": "Last name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Gender",
      "options": {
        male: 'Male',
        female: 'Female'
      },
      "type": "radio",
      "label": "Gender"
    },
    model: {
      value: _vm.gender,
      callback: function ($$v) {
        _vm.gender = $$v;
      },
      expression: "gender"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "text",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "double-wide"
  }, [_c('FormulateInput', {
    attrs: {
      "name": "password",
      "type": "password",
      "label": "Password",
      "placeholder": "Your password",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "password_confirm",
      "type": "password",
      "label": "Confirm your password",
      "placeholder": "Confirm password",
      "validation": "required|confirm",
      "validation-name": "Confirmation"
    }
  })], 1), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 1);
};

var __vue_staticRenderFns__ = [];
/* style */

const __vue_inject_styles__ = function (inject) {
  if (!inject) return;
  inject("data-v-71cbb2b8_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__ = undefined;
/* module identifier */

const __vue_module_identifier__ = undefined;
/* functional template */

const __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

//
var script$1 = {
  name: 'BuyerBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function () {
    return {
      gender: '',
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit(data) {
      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      const farmerData = {
        username: data['username'],
        sureName: data['surname'],
        firstName: data['First Name'],
        middleName: data['Middle name'],
        gender: data['Gender'],
        address: data['Address'],
        phoneNumber: data['phone no'],
        email: data['email'],
        password: data['password'],
        password_confirmation: data['password_confirm'],
        title: data['title'],
        client_key: this.clientKey
      };
      appCommonService.postData(this.serverUrl + '/api/clientbuyers', farmerData).then(response => {
        console.log(response);
        this.responseFarmerId = response.farmer_id;
        this.showForm = false;
        this.showResponse = true;
        this.disabledBtn = false;
        this.submitText = 'Submit';
      }).catch(error => {
        console.log(error.response.data.errors);
        this.disabledBtn = false;
        this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              this.mapErrors(error.response.data.errors);
          }
        }
      });
    },

    mapErrors(errors) {
      this.formErrors = [];

      for (const [key, val] of Object.entries(errors)) {
        this.formErrors.push(val[0]);
      }
    }

  }
};

/* script */
const __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showResponse,
      expression: "showResponse"
    }],
    staticClass: "form-wrapper"
  }, [_c('h3', [_vm._v("Profile was created ")])]), _vm._v(" "), _c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    },
    model: {
      value: _vm.formData,
      callback: function ($$v) {
        _vm.formData = $$v;
      },
      expression: "formData"
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Buyer sign up form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "label": "Title",
      "name": "title",
      "placeholder": "Title",
      "type": "select",
      "options": [{
        value: 'Alh',
        label: 'Alhaji'
      }, {
        value: 'Mr',
        label: 'Mr.'
      }, {
        value: 'Mrs',
        label: 'Mrs.'
      }, {
        value: 'Miss',
        label: 'Miss'
      }, {
        value: 'Mal',
        label: 'Mallam'
      }],
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "First Name",
      "type": "text",
      "label": "First name",
      "placeholder": "First name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Middle name",
      "type": "text",
      "label": "Middle name",
      "placeholder": "Middle name"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "surname",
      "type": "text",
      "label": "Last name",
      "placeholder": "Last name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Gender",
      "options": {
        male: 'Male',
        female: 'Female'
      },
      "type": "radio",
      "label": "Gender"
    },
    model: {
      value: _vm.gender,
      callback: function ($$v) {
        _vm.gender = $$v;
      },
      expression: "gender"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "text",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "username",
      "type": "text",
      "label": "Username",
      "placeholder": "Username",
      "validation": "required"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "double-wide"
  }, [_c('FormulateInput', {
    attrs: {
      "name": "password",
      "type": "password",
      "label": "Password",
      "placeholder": "Your password",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "password_confirm",
      "type": "password",
      "label": "Confirm your password",
      "placeholder": "Confirm password",
      "validation": "required|confirm",
      "validation-name": "Confirmation"
    }
  })], 1), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 1);
};

var __vue_staticRenderFns__$1 = [];
/* style */

const __vue_inject_styles__$1 = function (inject) {
  if (!inject) return;
  inject("data-v-25a704ed_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__$1 = undefined;
/* module identifier */

const __vue_module_identifier__$1 = undefined;
/* functional template */

const __vue_is_functional_template__$1 = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$1 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, createInjector, undefined, undefined);

//
const INPUT_PROVIDER = 'INPUT_PROVIDER',
      OFFTAKER = 'OFF_TAKER',
      MECHANIZATION = 'MECHANIZATION_PROVIDER';
var script$2 = {
  name: 'ServiceProviderBaseForm',
  props: {
    clientKey: '',
    serverUrl: ''
  },
  data: function () {
    return {
      formData: {},
      formErrors: [],
      submitText: 'Submit',
      operations: [],
      disabledBtn: false,
      showForm: true,
      showResponse: false
    };
  },
  methods: {
    handleSubmit(data) {
      this.disabledBtn = true;
      this.submitText = 'Submiting...';
      const farmerData = {
        name: data['company name'],
        address: data['Address'],
        phone: data['phone no'],
        email: data['email'],
        client_key: this.clientKey
      };

      for (const operation of this.operations) {
        if (operation == INPUT_PROVIDER) {
          farmerData.input_provider = INPUT_PROVIDER;
        }

        if (operation == OFFTAKER) {
          farmerData.off_taker = OFFTAKER;
        }

        if (operation == MECHANIZATION) {
          farmerData.mechanization = MECHANIZATION;
        }
      }

      appCommonService.postData(this.serverUrl + '/api/clientserviceproviders', farmerData).then(response => {
        console.log(response);
        this.responseFarmerId = response.farmer_id;
        this.showForm = false;
        this.showResponse = true;
        this.disabledBtn = false;
        this.submitText = 'Submit';
      }).catch(error => {
        console.log(error.response.data.errors);
        this.disabledBtn = false;
        this.submitText = 'Submit';

        if (error.response && error.response.status) {
          switch (error.response.status) {
            case 422:
              this.mapErrors(error.response.data.errors);
          }
        }
      });
    },

    mapErrors(errors) {
      this.formErrors = [];

      for (const [key, val] of Object.entries(errors)) {
        this.formErrors.push(val[0]);
      }
    }

  }
};

/* script */
const __vue_script__$2 = script$2;
/* template */

var __vue_render__$2 = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showResponse,
      expression: "showResponse"
    }],
    staticClass: "form-wrapper"
  }, [_c('h3', [_vm._v("Company profile was created ")])]), _vm._v(" "), _c('div', [_c('FormulateForm', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: _vm.showForm,
      expression: "showForm"
    }],
    staticClass: "form-wrapper",
    attrs: {
      "form-errors": _vm.formErrors
    },
    on: {
      "submit": _vm.handleSubmit
    }
  }, [_c('h3', {
    staticClass: "form-title"
  }, [_vm._v("Service provider sign up form")]), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "company name",
      "type": "text",
      "label": "Company Name",
      "placeholder": "Company name",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "Address",
      "type": "textarea",
      "label": "Address",
      "placeholder": "Address",
      "validation": "required"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "phone no",
      "type": "text",
      "label": "Phone No",
      "placeholder": "Phone no",
      "validation": "^required|matches:/[0-9]/"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "name": "email",
      "type": "email",
      "label": "Email address",
      "placeholder": "Email address",
      "validation": "required|email"
    }
  }), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "options": {
        INPUT_PROVIDER: 'INPUT PROVIDERS',
        'OFF_TAKER': 'OFFTAKER',
        'MECHANIZATION_PROVIDER': 'MECHANIZATION PROVIDER'
      },
      "type": "checkbox",
      "label": "Company operation(s)"
    },
    model: {
      value: _vm.operations,
      callback: function ($$v) {
        _vm.operations = $$v;
      },
      expression: "operations"
    }
  }), _vm._v(" "), _c('FormulateErrors'), _vm._v(" "), _c('FormulateInput', {
    attrs: {
      "type": "submit",
      "label": _vm.submitText,
      "disabled": _vm.disabledBtn
    }
  })], 1)], 1)]);
};

var __vue_staticRenderFns__$2 = [];
/* style */

const __vue_inject_styles__$2 = function (inject) {
  if (!inject) return;
  inject("data-v-b89d7a94_0", {
    source: ".form-wrapper{padding:2em;border:1px solid #a8a8a8;border-radius:.5em;max-width:500px;box-sizing:border-box;margin:0 auto}.form-title{margin-top:0}.form-wrapper::v-deep .formulate-input .formulate-input-element{max-width:none}@media (min-width:420px){.double-wide{display:flex}.double-wide .formulate-input{flex-grow:1;width:calc(50% - .5em)}.double-wide .formulate-input:first-child{margin-right:.5em}.double-wide .formulate-input:last-child{margin-left:.5em}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


const __vue_scope_id__$2 = undefined;
/* module identifier */

const __vue_module_identifier__$2 = undefined;
/* functional template */

const __vue_is_functional_template__$2 = false;
/* style inject SSR */

/* style inject shadow dom */

const __vue_component__$2 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$2,
  staticRenderFns: __vue_staticRenderFns__$2
}, __vue_inject_styles__$2, __vue_script__$2, __vue_scope_id__$2, __vue_is_functional_template__$2, __vue_module_identifier__$2, false, createInjector, undefined, undefined);

/* eslint-disable import/prefer-default-export */

var components = /*#__PURE__*/Object.freeze({
    __proto__: null,
    FarmersBaseForm: __vue_component__,
    BuyerBaseForm: __vue_component__$1,
    ServiceProviderBaseForm: __vue_component__$2
});

//import '../themes/snow/snow.scss'

const install = function installRastechForms(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(([componentName, component]) => {
    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


const plugin = {
  install
}; // To auto-install on non-es builds, when vue is found

export default plugin;
export { __vue_component__$1 as BuyerBaseForm, __vue_component__ as FarmersBaseForm, __vue_component__$2 as ServiceProviderBaseForm };
